//pure function
//side effect: c = 10
const c = 10
function add(a , b) {
    return a + b + c;
} 
const result = add(1,6);
console.log("Pure function:", result);