function* gen() {
    yield "Mark";
    yield "Elon";
    yield "Bill";
}

const mygen = gen();
console.log(mygen.next().value);
console.log(mygen.next().value);
console.log(mygen.next().value);