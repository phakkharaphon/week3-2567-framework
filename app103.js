//outer function
function greet() {
    const name = 'Mr.J Doe'
    //inner function
    function displayName() {
        return "Hi:"+" "+name
    }
    return displayName
}
const gi = greet();
console.log(gi)
console.log(gi())
