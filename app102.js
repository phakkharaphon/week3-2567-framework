function a() {
    console.log("Callback function")
}
//Nested function
function SayHi(Callback,fname, lname) {
    Callback();
    function getfullname() {
        return fname +" " + lname
    }
    return getfullname()
}
const message = SayHi("Mr.mark","Zackerberg")
console.log(message)